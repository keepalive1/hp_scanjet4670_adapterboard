This is a very simple adapter board that converts the HP Scanjet 4670 see-trough scanners 
mini DIN connector to a 12V DC Jack and a USB 2.0 Port to make it possible to connect it 
easily without original cables. 

The original Driver CD ISO image for the scanner can be obtained from archive.org
from this link https://archive.org/details/hp-scanjet-4670 
 
Have fun and don't throw away great hardware, make it work, keep it alive.
